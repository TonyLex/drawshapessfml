#QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
    LIBS += -LC:/Users/lekhnovich.av/Documents/DrawShapes/drawshapessfml/SFML-2.5.1/lib
    #INCLUDEPATH += {PWD}SFML-2.5.1/include
    INCLUDEPATH += C:/Users/lekhnovich.av/Documents/DrawShapes/drawshapessfml/SFML-2.5.1/include


}

CONFIG(release, debug|release) {
    win32:LIBS += -lsfml-graphics -lsfml-window -lsfml-system
}

CONFIG(debug, debug|release) {
    win32:LIBS += -lsfml-graphics-d -lsfml-window-d -lsfml-system-d
}


SOURCES += \
    main.cpp

HEADERS += \
    Shapes/ShapeManager.h \
    Shapes/Shapes.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
