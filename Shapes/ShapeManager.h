#ifndef SHAPEMANAGER_H
#define SHAPEMANAGER_H

#include <Shapes/Shapes.h>

class ShapeManager {
public:
    void addShape(std::shared_ptr<Shape> shape) {
        m_shapes.push_back(shape);
    }

    void removeLastShape() {
        if (!m_shapes.empty()) {
            m_shapes.pop_back();
        }
    }

    void clearSelected() {
        for (auto& shape : m_shapes) {
            shape->setSelected(false);
        }
    }

    void draw(sf::RenderWindow& window) {
        for (auto& shape : m_shapes) {
            shape->draw(window);
        }
    }

    void checkSelection(int x, int y) {
        for (auto& shape : m_shapes) {
            if (shape->contains(sf::Vector2f(x, y))) {
                shape->setSelected(true);
            } else {
                shape->setSelected(false);
            }
        }
    }

    void checkGroupSelection(const sf::FloatRect& selectionArea) {
        for (auto& shape : m_shapes) {
            if (shape->intersects(selectionArea)) {
                shape->setSelected(true);
            } else {
                shape->setSelected(false);
            }
        }
    }

    void moveSelectedShape(float dx, float dy){
        for (const auto& shape : m_shapes) {
            if (shape->isSelected()) {
                shape->move(dx, dy);
            }
        }
    }

    ~ShapeManager() {
    }

private:
    std::vector<std::shared_ptr<Shape>> m_shapes;
};

#endif // SHAPEMANAGER_H
