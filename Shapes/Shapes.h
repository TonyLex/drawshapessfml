#ifndef SHAPES_H
#define SHAPES_H

#include <SFML-2.5.1/include/SFML/Graphics.hpp>
#include <vector>

class Shape {
public:
    virtual void draw(sf::RenderWindow& window) = 0;
    virtual ~Shape() {}

    virtual bool contains(const sf::Vector2f& point) const = 0;
    virtual bool intersects(const sf::FloatRect& rect) const = 0;
    virtual void setSelected(bool selected) = 0;
    virtual void move(float dx, float dy) = 0;
    virtual bool isSelected() const = 0;
};

class Circle : public Shape {
private:
    double radius;
    sf::CircleShape circleShape;
public:
    Circle(int x, int y, int radius, const sf::Color& color) : radius(radius) {
        circleShape.setRadius(radius);
        circleShape.setPosition(x, y);
        circleShape.setFillColor(color);
    }

    void draw(sf::RenderWindow& window) override {
        window.draw(circleShape);
    }

    bool contains(const sf::Vector2f& point) const override {
        float distance = sqrt(pow(point.x - circleShape.getPosition().x, 2) + pow(point.y - circleShape.getPosition().y, 2));
        return distance <= radius;
    }

    bool intersects(const sf::FloatRect& rect) const override {
        // Простая проверка на пересечение (не точная)
        return circleShape.getGlobalBounds().intersects(rect);
    }

    void setSelected(bool selected) override {
        if (selected) {
            circleShape.setOutlineColor(sf::Color::Blue);
            circleShape.setOutlineThickness(2.0f);
        } else {
            circleShape.setOutlineThickness(0.0f);
        }
    }

    void move(float dx, float dy) override {
            circleShape.move(dx, dy);
    }

    bool isSelected() const override {
        return circleShape.getOutlineThickness() != 0.0f;
    }
};

class Triangle : public Shape {
private:
    sf::ConvexShape triangleShape;
public:
    Triangle(int x, int y, int side, const sf::Color& color) {
        triangleShape.setPointCount(3);
        triangleShape.setPoint(0, sf::Vector2f(x, y));
        triangleShape.setPoint(1, sf::Vector2f(x + side / 2, y - side * sqrt(3) / 2));
        triangleShape.setPoint(2, sf::Vector2f(x + side, y));
        triangleShape.setFillColor(color);
    }

    void draw(sf::RenderWindow& window) override {
        window.draw(triangleShape);
    }

    bool contains(const sf::Vector2f& point) const override {
        sf::Vector2f v0 = triangleShape.getPoint(0);
        sf::Vector2f v1 = triangleShape.getPoint(1);
        sf::Vector2f v2 = triangleShape.getPoint(2);

        // Вычисление площади исходного треугольника
        float area = 0.5f * fabs((v0.x - v2.x) * (v1.y - v0.y) - (v0.x - v1.x) * (v2.y - v0.y));

        // Вычисление площади подтреугольников, образованных точкой и вершинами исходного треугольника
        float area1 = 0.5f * fabs((point.x - v1.x) * (v0.y - point.y) - (point.x - v0.x) * (v1.y - point.y));
        float area2 = 0.5f * fabs((point.x - v2.x) * (v1.y - point.y) - (point.x - v1.x) * (v2.y - point.y));
        float area3 = 0.5f * fabs((point.x - v0.x) * (v2.y - point.y) - (point.x - v2.x) * (v0.y - point.y));

        // Если сумма площадей подтреугольников равна площади исходного треугольника, точка находится внутри треугольника
        return (area == area1 + area2 + area3);
    }


    bool intersects(const sf::FloatRect& rect) const override {
        return triangleShape.getGlobalBounds().intersects(rect);
    }

    void setSelected(bool selected) override {
        if (selected) {
            triangleShape.setOutlineColor(sf::Color::Blue);
            triangleShape.setOutlineThickness(2.0f);
        } else {
            triangleShape.setOutlineThickness(0.0f);
        }
    }

    void move(float dx, float dy) override {
            triangleShape.move(dx, dy);
    }

    bool isSelected() const override {
        return triangleShape.getOutlineThickness() != 0.0f;
    }

};

class Rectangle : public Shape {
private:
    double width;
    double height;
    sf::RectangleShape rectangleShape;
public:
    Rectangle(int x, int y, int width_l, int height_l, const sf::Color& color): width(width_l), height(height_l) {
        rectangleShape.setSize(sf::Vector2f(width, height));
        rectangleShape.setPosition(x, y);
        rectangleShape.setFillColor(color);
    }

    void draw(sf::RenderWindow& window) override {
        window.draw(rectangleShape);
    }

    bool contains(const sf::Vector2f& point) const override {
        return rectangleShape.getGlobalBounds().contains(point);
    }

    bool intersects(const sf::FloatRect& rect) const override {
        return rectangleShape.getGlobalBounds().intersects(rect);
    }

    void setSelected(bool selected) override {
        if (selected) {
            rectangleShape.setOutlineColor(sf::Color::Blue);
            rectangleShape.setOutlineThickness(2.0f);
        } else {
            rectangleShape.setOutlineThickness(0.0f);
        }
    }

    void move(float dx, float dy) override {
            rectangleShape.move(dx, dy);
    }

    bool isSelected() const override {
        return rectangleShape.getOutlineThickness() != 0.0f;
    }

};



#endif // SHAPES_H

