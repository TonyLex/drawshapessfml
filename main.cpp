#include <Shapes/ShapeManager.h>

#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

int main() {
    srand(static_cast<unsigned>(time(0)));

    sf::RenderWindow window(sf::VideoMode(800, 600), "Geometric Shapes");
    window.setFramerateLimit(60);

    ShapeManager shapeManager;

    bool isSelecting = false;
    sf::Vector2i selectionStart;

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();

            // Обработка нажатия клавиш
            // ...

            if (event.type == sf::Event::KeyPressed) {
                float moveDistance = 5.0f; // Расстояние, на которое будет перемещаться фигура
                switch (event.key.code) {
                    case sf::Keyboard::Up:
                        shapeManager.moveSelectedShape(0, -moveDistance);
                        break;
                    case sf::Keyboard::Down:
                        shapeManager.moveSelectedShape(0, moveDistance);
                        break;
                    case sf::Keyboard::Left:
                        shapeManager.moveSelectedShape(-moveDistance, 0);
                        break;
                    case sf::Keyboard::Right:
                        shapeManager.moveSelectedShape(moveDistance, 0);
                        break;
                    default:
                        break;
                }
            }

            float x = 100.0f;
            float y = 100.0f;
            float radius = 50.0f;
            float width = 100.0f;
            float height = 50.0f;
            float side = 60.0f;
            sf::Color color = sf::Color::Red;

            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::C) {
                    // Создание круга
                    std::shared_ptr<Shape> circle = std::make_shared<Circle>(x, y, radius, color);
                    shapeManager.addShape(circle);
                } else if (event.key.code == sf::Keyboard::R) {
                    // Создание прямоугольника
                    std::shared_ptr<Shape> rectangle = std::make_shared<Rectangle>(x, y, width, height, color);
                    shapeManager.addShape(rectangle);
                } else if (event.key.code == sf::Keyboard::T) {
                    // Создание треугольника
                    std::shared_ptr<Shape> triangle = std::make_shared<Triangle>(x, y, side, color);
                    shapeManager.addShape(triangle);
                }
            }


            // Обработка нажатия мыши
            if (event.type == sf::Event::MouseButtonPressed) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    isSelecting = true;
                    selectionStart = sf::Mouse::getPosition(window);
                    shapeManager.clearSelected();
                }
            }

            if (event.type == sf::Event::MouseButtonReleased) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    isSelecting = false;
                    sf::Vector2i selectionEnd = sf::Mouse::getPosition(window);
                    sf::FloatRect selectionArea(
                        std::min(selectionStart.x, selectionEnd.x),
                        std::min(selectionStart.y, selectionEnd.y),
                        abs(selectionEnd.x - selectionStart.x),
                        abs(selectionEnd.y - selectionStart.y)
                    );
                    shapeManager.checkGroupSelection(selectionArea);
                }
            }

        }

        window.clear();
        shapeManager.draw(window);

        if (isSelecting) {
            sf::Vector2i mousePos = sf::Mouse::getPosition(window);
            sf::RectangleShape selectionRect;
            selectionRect.setPosition(sf::Vector2f(std::min(selectionStart.x, mousePos.x), std::min(selectionStart.y, mousePos.y)));
            selectionRect.setSize(sf::Vector2f(abs(mousePos.x - selectionStart.x), abs(mousePos.y - selectionStart.y)));
            selectionRect.setFillColor(sf::Color(0, 0, 255, 50));
            selectionRect.setOutlineColor(sf::Color::Blue);
            selectionRect.setOutlineThickness(1);
            window.draw(selectionRect);
        }

        window.display();
    }

    return 0;
}
